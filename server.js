require('dotenv').config();
const express = require('express');
const Keycloak = require('keycloak-connect');
const session = require('express-session');
const expressHbs = require('express-handlebars');
const bodyParser = require('body-parser');
const app = express();
const host = process.env.APP_HOST;
const port = process.env.APP_PORT;
const route = require('./router');
const expressSwagger = require('express-swagger-generator')(app);

// Register 'handelbars' extension with The Mustache Express
app.engine('hbs', expressHbs({extname:'hbs',
  defaultLayout:'layout.hbs',
  relativeTo: __dirname}));
app.set('view engine', 'hbs');

var memoryStore = new session.MemoryStore();
var keycloak = new Keycloak({ store: memoryStore });

//session
app.use(session({
    secret:'thisShouldBeLongAndSecret',
    resave: false,
    saveUninitialized: true,
    store: memoryStore
}));
  
app.use(keycloak.middleware());

app.use(bodyParser.json());
app.use(
    bodyParser.urlencoded({
        extended: true,
    })
);

app.use( keycloak.middleware( { logout: '/'} ));

route(app, keycloak);

let options = {
    swaggerDefinition: {
        info: {
            description: 'This is a sample server',
            title: 'Swagger',
            version: '1.0.0',
        },
        host: `${host}:${port}`,
        basePath: '/v1',
        produces: [
            "application/json",
            "application/xml"
        ],
        schemes: ['http', 'https'],
        securityDefinitions: {
            JWT: {
                type: 'apiKey',
                in: 'header',
                name: 'Authorization',
                description: "",
            }
        }
    },
    basedir: __dirname, //app absolute path
    files: ['./router.js'] //Path to the API handle folder
};
expressSwagger(options);

app.listen(port, host,() => {
    console.log(`Server running on ${host}:${port}.`)
})

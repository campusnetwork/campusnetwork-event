const db = require('./query');
const express = require('express');
const app = express();

const InitializeRoute = (app, keycloak)=>{
    /**
     * This function comment is parsed by doctrine
     * @route GET /events
     * @group foo - Events information
     * @returns {object} 200 - An array of event info
     * @returns {Error}  default - Unexpected error
     */
    app.get('/events', keycloak.protect(), db.getEvents);
    /**
     * This function comment is parsed by doctrine
     * @route GET /events/:id
     * @group foo - Event information
     * @returns {object} 200 - A event info
     * @returns {Error}  default - Unexpected error
     */
    app.get('/events/:id', keycloak.protect(), db.getEventById);
    /**
     * This function comment is parsed by doctrine
     * @route delete /events/:id
     * @group foo - delete event
     */
    app.delete('/events/:id', keycloak.protect(), db.deleteEvent);
    /**
     * This function comment is parsed by doctrine
     * @route post /events
     * @group foo - Create event
     */
    app.post('/events', keycloak.protect(), db.createEvent);
    /**
     * This function comment is parsed by doctrine
     * @route put /events/:id
     * @group foo - update event
     */
    app.put('/events/:id', keycloak.protect(), db.updateEvent);
    /**
     * 
     */
    app.get('/events/:id/comments/:limit', keycloak.protect(), db.getLastComments);
    /**
     * 
     */
    app.post('/events/addComment', keycloak.protect(), db.addComments);
    /**
     * 
     */
    app.get('/events/:id/nbcomments', keycloak.protect(), db.getNbComments);
    /**
     * 
     */
    app.get('/events/:id/likes', keycloak.protect(), db.getLike);
    /**
     * 
     */
    app.post('/events/:id/likes', keycloak.protect(), db.setLikes);
    /**
     * 
     */
    app.delete('/events/:id/likes', keycloak.protect(), db.removeLike);
}
module.exports= InitializeRoute;

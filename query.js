require('dotenv').config();
const Pool = require('pg').Pool
const pool = new Pool({
    user: process.env.DB_USER,
    host: process.env.DB_HOST,
    database: process.env.DB_NAME,
    password: DB_PASS,
    port: DB_PORT,
})

const getEvents = (request, response) => {
    const page = parseInt(request.query.page);
    pool.query('SELECT *, (SELECT COUNT(*) FROM event_db."like" WHERE event_db."like".eventid = event_db."event".id) AS likes ' +
        'FROM event_db."event" LIMIT 5 OFFSET ($1) * 5 ORDER BY event_db."event".datebegin DESC', [page],
        (error, eventsResults) => {
            let events = eventsResults.rows;
            const listIdEvent = events.map(event => event.id);
            pool.query('SELECT * from event_db."comment" WHERE event_db."comment".eventid = ANY ($1) LIMIT 5', [listIdEvent],
                (error, commentsResults) => {
                    if (error) {
                        console.error(error)
                    }
                    events.map(event => {
                        event['comments'] = [];
                        commentsResults.rows.map(comment => {
                            if (comment.eventid === event.id) {
                                event['comments'].push(comment);
                            }
                        })
                    })
                    response.status(200).json(events);
                }
            )
        }
    );
}

const getEventById = (request, response) => {
    const id = request.params.id;

    pool.query('SELECT *, (SELECT COUNT(*) FROM event_db."like" WHERE event_db."like".eventid = event_db."event".id) AS likes ' +
    'FROM event_db."event" WHERE id = $1', [id], (error, results) => {
        if (error) {
            console.error(error)
        }
        pool.query
        response.status(200).json(results.rows)
    })
}

const deleteEvent = (request, response) => {
    const id = parseInt(request.params.id)

    pool.query('DELETE FROM event_db."event" WHERE id = $1', [id], (error, results) => {
        if (error) {
            console.error(error)
        }
        response.status(200).send(`Event deleted with ID: ${id}`)
    })
}

const createEvent = (request, response) => {
    const {id, title, content, datebegin, dateend ,datecreated,authorid} = request.body

    pool.query('INSERT INTO event_db."event" (id,title, content, datebegin, dateend, datecreated, authorid) VALUES ($1, $2, $3, $4, $5, $6,$7)',
    [id,title, content, datebegin, dateend, datecreated,authorid],
    (error, results) => {
        if (error) {
            console.error(error)
        }
        response.status(201).send(`Event added with ID: ${results}`)
    })
}

const updateEvent = (request, response) => {
    const id = parseInt(request.params.id)
    const { titre, description, dateEvenement, dateSuppression, photoCouverture } = request.body

    pool.query(
        'UPDATE event_db."event" SET titre = $2, description = $3, dateEvenement = $4, dateSuppression = $5, photoCouverture = $6 WHERE id = $1',
        [id, titre, description, dateEvenement, dateSuppression, photoCouverture],
        (error, results) => {
            if (error) {
                console.error(error)
            }
            response.status(200).send(`Event modified with ID: ${id}`)
        }
    )
}

const getLastComments = (request, response) => {
    const id = request.params.id;
    const limit = parseInt(request.params.limit);
    const page = parseInt(request.query.page);
    
    pool.query(
        'SELECT * FROM event_db.COMMENT WHERE eventid = ($1) ORDER BY datecreated DESC ' + 
        'LIMIT 5 OFFSET $2 * 5',
        [id, page],
        (error, results) => {
            if (error) {
                throw error
            }
            response.status(200).json(results.rows)
        }
    )
}

const addComments = (request, response) => {
    const { uuid, content, datecreated, authorid, eventid } = request.body;

    pool.query(
        'INSERT INTO event_db.comment (id, content, datecreated, authorid, eventid) VALUES ($1, $2, $3, $4, $5)',
        [uuid, content, datecreated, authorid, eventid],
        (error, results) => {
            if (error) {
                throw error
            }
            response.status(201).json(results.rows[0])
        }
    )
}

const getNbComments = (request, response) => {
    const eventid = request.params.id;

    pool.query(
        'SELECT COUNT(*) FROM event_db.comment WHERE event_db.comment.eventid = ($1)',
        [eventid],
        (error, results) => {
            if (error) {
                throw error
            }
            response.status(201).json(results.rows[0])
        }
    )
}

const setLikes = (request, response) => {
    const eventid = request.params.id;
    const { userid } = request.body;

    pool.query(
        'INSERT INTO event_db.like (eventid, userid) VALUES ($1, $2)',
        [eventid, userid],
        (error, results) => {
            if (error) {
                throw error
            }
            response.status(201).json(results.rows)
        }
    )
}

const removeLike = (request, response) => {
    const eventid = request.params.id;
    const userid = request.query.userid;

    pool.query(
        'DELETE FROM event_db.like WHERE eventid = ($1) AND userid = ($2)',
        [eventid, userid],
        (error, results) => {
            if (error) {
                throw error
            }
            response.status(200).json()
        }
    )
}

const getLike = (request, response) => {
    const eventid = request.params.id;
    const userid = request.query.userid;

    pool.query(
        'SELECT * FROM event_db.like WHERE eventid = ($1) AND userid = ($2)',
        [eventid, userid],
        (error, results) => {
            if (error) {
                throw error
            }
            response.status(200).json(results.rows)
        }
    )
}

module.exports = {
    getEvents,
    getEventById,
    deleteEvent,
    createEvent,
    updateEvent,
    getLastComments,
    addComments,
    setLikes,
    getLike,
    removeLike,
    getNbComments,
}

# API Event

<img src="https://i.ibb.co/6R8FSVQ/sezegzaeg.png"></img>

CampusNetwork est un réseau social inter campus visant à simplifier les création d'évennements entre écoles et campus.

L'Api Event est une des apis du réseau social CampusNetwork. Il a pour but de récupérer les informations
relatives aux évennements dans l'application.

L'application est actuellement disponible sur https://event.campusnetwork.mohamedhenni.wtf/.

### Voir les autres autres application de CampusNetwork

 * (Front)[https://gitlab.com/campusnetwork/campusnetwork-front]
 * (Api-User)[https://gitlab.com/campusnetwork/campusnetwork-user]
 * (Api-Organization)[https://gitlab.com/campusnetwork/campus-organization]
 * (Api-Media)[https://gitlab.com/campusnetwork/campusnetwork-api-objectstorage]

## Premiers pas

### Pré requis

Tout d'abord il faut installer (npm et NodeJS)[https://nodejs.org/] pour pouvoir supporter l'application.

### Installation

Pour installer toutes les librairies dont à besoin l'application pour fonctionner correctement, lancer

```
npm install
```

Ensuite lancer

```
npm start
```

Qui lancera le script ci-dessous permettant de lancer l'Api (sur le port 3000 par défaut, 3001 conseillé pour correspondre au (Front CampusNetwork)[https://gitlab.com/campusnetwork/campusnetwork-front].

```
node server.js
```

## Configuration

Créer un fichier .env à la racine du projet contenant les informations de configuration du projet.

APP_HOST=  #127.0.0.1
APP_PORT=  #3001
DB_USER=   #databaseUser
DB_HOST=   #databaseHost
DB_PASS=   #databasePsswrd
DB_PORT=   #databasePort
DB_NAME=   #databaseName

## Tests

Il n'y a actuellement pas de test implémentés pour cette application.

## Deploiement

Prêt à être conteneurisé avec (docker)[https://www.docker.com/]

Pour cela mettre la variable du .env APP_HOST=0.0.0.0

Puis lancer la commande depuis la racine du projet

```
docker build -t [imageName] .
```

Déployé sur un cluster (k3s)[https://k3s.io/]

## Créer avec

Les node modules:

* Body-parser
* Express
* Express-handlebars
* Express-sessions
* Express-swagger-generator
* Keycloak-connect

Remerciement aux créateurs et contributeurs de ces librairies.

## Authors

L'équipe Campus Network :
 * Berthomé Théo
 * Doucet Gregory
 * Métrot Corentin
